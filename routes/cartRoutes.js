const express = require("express");
const router = express.Router();
const CartController = require("../controllers/CartController.js");
const auth = require("../auth.js");

//Add single product
router.post("/add-product", auth.verify, (request, response) => {
	CartController.addProductToCart(request, response);
});

//Remove single product
router.post("/remove-product", auth.verify, (request, response) => {
	CartController.removeProductFromCart(request, response);
});

//Change single product quantities
router.put("/change-quantity", auth.verify, (request, response) => {
	CartController.changeQuantity(request, response);
});

//Get subtotal for each item
router.get("/subtotal", auth.verify, (request, response) => {
	CartController.getSubtotal(request, response);
});

//Get total price for all items
router.get("/total-price", auth.verify, (request, response) => {
	CartController.getTotalPrice(request, response);
});

//Get cart details
router.get("/1", auth.verify, (request, response) => {
	CartController.getCart(request, response);
});

//Cart checkout
router.get("/checkout", auth.verify, (request, response) => {
	CartController.checkoutCart(request, response);
});

module.exports = router;