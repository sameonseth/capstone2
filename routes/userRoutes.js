const express = require("express");
const router = express.Router();
const UserController = require("../controllers/UserController.js");
const auth = require("../auth.js");

//Register user
router.post("/register", (request, response) => {
	UserController.registerUser(request, response);
});

//Login user
router.post("/login", (request, response) => {
	UserController.loginUser(request, response);
});

//Retrieve all users
router.get("/all", (request, response) => {
	UserController.getAllUsers(request, response);
});

//Retrieve user details
router.get("/profile", auth.verify, (request, response) => {
	UserController.getProfile(request, response);
});

//Set user as admin
router.put("/:id/admin", auth.verify, auth.verifyAdmin, (request, response) => {
	UserController.setUserAsAdmin(request, response);
});

//Set admin as user
router.put("/:id/user", auth.verify, auth.verifyAdmin, (request, response) => {
	UserController.setAdminAsUser(request, response);
});

//Change user's password
router.put("/new-password", auth.verify, (request, response) => {
	UserController.changePassword(request, response);
});

module.exports = router;