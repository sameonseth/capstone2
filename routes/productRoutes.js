const express = require("express");
const router = express.Router();
const ProductController = require("../controllers/ProductController.js");
const auth = require("../auth.js");

//Create product
router.post("/", auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.addProduct(request, response);
});

//Retrieve all products
router.get("/all", (request, response) => {
	ProductController.getAllProducts(request, response);
});

//Retrive all active products
router.get("/active", (request, response) => {
	ProductController.getAllActiveProducts(request, response);
});

//Retrieve single product
router.get("/:id", (request, response) => {
	ProductController.getProduct(request, response);
});

//Update product information
router.put("/:id", auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.updateProduct(request, response);
});

//Archive product
router.put("/:id/archive", auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.archiveProduct(request, response);
});

//Activate product
router.put("/:id/activate", auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.activateProduct(request, response);
});

//Add stock
router.put("/:id/add-stock", auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.addStock(request, response);
});

//Reduce stock
router.put("/:id/reduce-stock", auth.verify, auth.verifyAdmin, (request, response) => {
	ProductController.reduceStock(request, response);
});

module.exports = router;