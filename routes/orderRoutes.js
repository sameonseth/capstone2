const express = require("express");
const router = express.Router();
const OrderController = require("../controllers/OrderController.js");
const auth = require("../auth.js");

//Create order
router.post("/new-order", auth.verify, (request, response) => {
	OrderController.createOrder(request, response);
});

//Retrieve authenticated user's orders
router.get("/", auth.verify, (request, response) => {
	OrderController.getCurrentUserOrders(request, response);
});;

//Retrieve all orders
router.get("/all", auth.verify, auth.verifyAdmin, (request, response) => {
	OrderController.getAllUsersOrders(request, response);
});;

module.exports = router;