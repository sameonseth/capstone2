//Server variables
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
require("dotenv").config();

const port = 4000;
const app = express();
const userRoutes = require("./routes/userRoutes.js");
const productRoutes = require("./routes/productRoutes.js");
const orderRoutes = require("./routes/orderRoutes.js");
const cartRoutes = require("./routes/cartRoutes.js");


//Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());


//Routes
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);
app.use("/carts", cartRoutes);


//Database connection
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@303-sameon.zsbishq.mongodb.net/e-commerce-api?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});
mongoose.connection.on("error", () => console.log("Can't connect to database"));
mongoose.connection.on("open", () => console.log("Connected to MongoDB!"));


//Since we're hosting this API in the cloud, the port to be used should be flexible hence, the use of the process.env.PORT which will take the port that the cloud server uses if the 'port' variable isn't available.
app.listen(process.env.PORT || port, () => {
	console.log(`E-commerce API is now running at localhost:${process.env.PORT || port}`);
});


module.exports = app;