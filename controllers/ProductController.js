const Product = require("../models/Product.js");

module.exports.addProduct = (request, response) => {
	Product.findOne({name: request.body.name}).then(result => {
		if(result){
			return response.send({
				status: "EXISTING_PRODUCT"
			});
		}
		else{
			if(request.body.price <= 0){
				return response.send({
					status: "INVALID_PRICE"
				});
			}
			if(request.body.stock < 0){
				return response.send({
					status: "INVALID_STOCK"
				});
			}
			let new_product = new Product({
				image: request.body.image,
				name: request.body.name,
				description: request.body.description,
				price: request.body.price,
				stock: request.body.stock
			});
			if(new_product.stock == 0) new_product.isActive = false;
			else if(new_product.stock > 0) new_product.isActive = true;
			return new_product.save().then(saved_product => {
				return response.send({
					status: "OKAY"
				});
			}).catch(error => response.send(error));
		}
	}).catch(error => response.send(error));		
}

module.exports.getAllProducts = (request, response) => {
	Product.find({})
	.then(all_products => {
		if(all_products.length == 0){
			return response.send({
				status: "EMPTY_PRODUCTS"
			});
		}
		return response.send(all_products);
	})
	.catch(error => response.send(error));
}

module.exports.getAllActiveProducts = (request, response) => {
	Product.find({isActive: true})
	.then(active_products => {
		if(active_products.length == 0){
			return response.send({
				status: "EMPTY_PRODUCTS"
			});
		}
		return response.send(active_products);
	})
	.catch(error => response.send(error));
}

module.exports.getProduct = (request, response) => {
	Product.findById(request.params.id)
	.then(product => response.send(product))
	.catch(error => response.send(error));
}

module.exports.updateProduct = (request, response) => {
	Product.findById(request.params.id).then(product => {
		if(product){
			if(request.body.price <= 0){
				return response.send({
					status: "INVALID_PRICE"
				});
			}
			if(request.body.stock < 0){
				return response.send({
					status: "INVALID_STOCK"
				});
			}
			product.image = request.body.image;
			product.name = request.body.name;
			product.description = request.body.description; 
			product.price = request.body.price;
			product.stock = request.body.stock;
			if(product.stock == 0) product.isActive = false;
			else if(product.stock > 0) product.isActive = true;
			return product.save().then(updated_product => {
				return response.send({
					status: "OKAY"
				});
			}).catch(error => response.send(error));
		}
		else{
			return response.send({
				status: "INVALID_PRODUCT"
			});
		}
	}).catch(error => response.send(error));
}

module.exports.archiveProduct = (request, response) => {
	Product.findById(request.params.id).then(product => {
		if(product){
			product.isActive = false;
			return product.save().then(archived_product => {
				return response.send({
					status: "OKAY"
				});
			}).catch(error => response.send(error));
		}
		else{
			return response.send({
				status: "INVALID_PRODUCT"
			});
		}
	}).catch(error => response.send(error));
}

module.exports.activateProduct = (request, response) => {
	Product.findById(request.params.id).then(product => {
		if(product){
			if(product.stock == 0){
				return response.send({
					status: "EMPTY_STOCK"
				});
			}
			product.isActive = true;
			return product.save().then(activated_product => {
				return response.send({
					status: "OKAY"
				});
			}).catch(error => response.send(error));
		}
		else{
			return response.send({
				status: "INVALID_PRODUCT"
			});
		}
	}).catch(error => response.send(error));
}

module.exports.addStock = (request, response) => {
	Product.findById(request.params.id).then(product => {
		if(product){
			if(request.body.stock <= 0){
				return response.send({
					message: "Invalid stock value."
				});
			}
			product.stock += request.body.stock;
			product.isActive = true;
			return product.save().then(activated_product => {
				return response.send({
					message: "Stock added successfully!"
				});
			}).catch(error => response.send(error));
		}
		else{
			return response.send({
				message: "The product is not in the database."
			});
		}
	}).catch(error => response.send(error));
}

module.exports.reduceStock = (request, response) => {
	Product.findById(request.params.id).then(product => {
		if(product){
			if(request.body.stock <= 0 || request.body.stock > product.stock){
				return response.send({
					message: "Invalid stock value."
				});
			}
			product.stock -= request.body.stock;
			if(product.stock == 0){
				product.isActive = false;
			}
			return product.save().then(activated_product => {
				return response.send({
					message: "Stock reduced successfully!"
				});
			}).catch(error => response.send(error));
		}
		else{
			return response.send({
				message: "The product is not in the database."
			});
		}
	}).catch(error => response.send(error));
}