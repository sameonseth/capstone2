const Cart = require("../models/Cart.js");
const User = require("../models/User.js");
const Product = require("../models/Product.js");
const Order = require("../models/Order.js");


module.exports.addProductToCart = async (request, response) => {
	//[SECTION] Check if product exists
	let this_product = await Product.findOne({_id: request.body.productId, isActive: true}).catch(error => response.send(error));
	if(!this_product){
		return response.send({
			message: "Product is not available."
		});
	}

	//[SECTION] Check if quantity input is more than zero
	if(request.body.quantity <= 0){
		return response.send({
			message: "Invalid quantity."
		});
	}

	//[SECTION] Check if quantity is more than the stocks
	if(request.body.quantity > this_product.stock){
		return response.send({
			message: "Stock is limited."
		});
	}

	//[SECTION] Check for any active cart
	Cart.find({userId: request.user.id, isActive: true}).then(async cart => {
		if(cart.length == 0){
			//[SECTION] Getting the fullname of the user with a certain id
			let user = await User.findOne({_id: request.user.id});
			let fullname = `${user.firstName} ${user.lastName}`;

			//[SECTION] Setting up temporary products array
		    let temp_products = [{
            	productId: request.body.productId,
            	productName: this_product.name,
            	quantity: request.body.quantity,
            	subTotal: this_product.price * request.body.quantity
            }];

		    //[SECTION] Create and save new cart
			let new_cart = new Cart({
				userId: request.user.id,
				userFullName: fullname,
				products: temp_products,
				totalPrice: temp_products[0].subTotal
			});
			return new_cart.save().then(saved_cart => {
				return response.send({
					message: "Product added to cart successfully!"
				});
			}).catch(error => response.send(error));
		}
		//[SECTION] Check if product is in cart and increment quantity if YES
		let isProductInCart = false;
		cart[0].products.forEach(productItem => {
			if(productItem.productId == request.body.productId){
				isProductInCart = true;
				productItem.quantity += request.body.quantity;
			}
		});

		//[SECTION] If NO, add new product to cart
		if(!isProductInCart){
			let new_cartItem = {
				productId: request.body.productId,
        		productName: this_product.name,
        		quantity: request.body.quantity,
        		subTotal: this_product.price * request.body.quantity
			}
			cart[0].products.push(new_cartItem);
		}

		//[SECTION] Calculate the total price
	    let totalValue = 0;
	    for(let productItem of cart[0].products){
	    	let current_product = await Product.findOne({_id: productItem.productId});
            if(current_product){
            	productItem.subTotal = current_product.price * productItem.quantity;
                totalValue += productItem.subTotal;
            }
	    }
	    cart[0].totalPrice = totalValue;

	    //[SECTION] Save the updated cart
		return cart[0].save().then(updated_cart => {
			return response.send({
				message: "Product added to cart successfully!"
			});
		}).catch(error => response.send(error));
	});
}


module.exports.removeProductFromCart = async (request, response) => {
	//[SECTION] Check if product exists
	let this_product = await Product.findOne({_id: request.body.productId, isActive: true}).catch(error => response.send(error));
	if(!this_product){
		return response.send({
			message: "Product is not available."
		});
	}

	//[SECTION] Check if quantity input is more than zero
	if(request.body.quantity <= 0){
		return response.send({
			message: "Invalid quantity."
		});
	}

	//[SECTION] Check for any active cart
	Cart.find({userId: request.user.id, isActive: true}).then(async cart => {
		if(cart.length == 0){
			return response.send({
				message: "The cart is empty."
			});
		}

		//[SECTION] Check if product is in cart and if the quantity input is too large; recalculate quantity, calculate subtotal, find index of the item when quantity is zero, and calculate total price
		let isProductInCart = false;
		let isQuantityTooLarge = false;
		let isQuantityZero = false;
		let indexToRemove = -1;
		let totalValue = 0;
		for(let productItem of cart[0].products){
			let current_product = await Product.findOne({_id: productItem.productId});
			if(productItem.productId == request.body.productId){
				isProductInCart = true;
				if(request.body.quantity > productItem.quantity) isQuantityTooLarge = true;
				else{
					productItem.quantity -= request.body.quantity;
					productItem.subTotal = current_product.price * productItem.quantity;
					if(productItem.quantity == 0){
						isQuantityZero = true;
						indexToRemove = cart[0].products.findIndex(product => product.productId == request.body.productId);
					}
				}
			}
			if(current_product){
				totalValue += productItem.subTotal;
			}
		}
		cart[0].totalPrice = totalValue;
		if(!isProductInCart){
			return response.send({
				message: "The product is not in the cart."
			});
		}
		if(isQuantityTooLarge){
			return response.send({
				message: "Invalid quantity."
			});
		}
		
		//[SECTION] Remove item from the cart when quantity is 0
		if(isQuantityZero){
			let newProductsArray = [];
			newProductsArray = newProductsArray.concat(
				cart[0].products.slice(0, indexToRemove),
				cart[0].products.slice(indexToRemove + 1)
			);
			cart[0].products = newProductsArray;
		}

		//[SECTION] Set inactive if the cart is empty
		if(cart[0].products.length == 0){
			cart[0].isActive = false;
		}

	    //[SECTION] Save the updated cart
		return cart[0].save().then(updated_cart => {
			return response.send({
				message: "Product removed from cart successfully!"
			});
		}).catch(error => response.send(error));
	});
}


module.exports.changeQuantity = async (request, response) => {
	Cart.find({userId: request.user.id, isActive: true}).then(async cart => {
		//[SECTION] Check if the cart is empty
		if(cart.length == 0){
			return response.send({
				message: "The cart is empty."
			});
		}
		
		//[SECTION] Check if the product is in the cart
		let isProductInCart = cart[0].products.some(productItem => {
			return (productItem.productId == request.body.productId && productItem);
		});
		if(!isProductInCart){
			return response.send({
				message: "The product is not in the cart."
			});
		}

		let this_product = await Product.findById(request.body.productId).catch(error => response.send(error));
		//[SECTION] Check if the quantity input is invalid
		if(request.body.quantity < 0 || request.body.quantity > this_product.stock){
			return response.send({
				message: "Invalid quantity."
			});
		}

		//[SECTION] Check if the quantity input is invalid
		else if(request.body.quantity > this_product.stock){
			return response.send({
				message: "Stock is limited."
			});
		}

		//[SECTION] Check if the quantity input is equal to zero
		else if(request.body.quantity == 0){
			let indexToRemove = cart[0].products.findIndex(product => product.productId == request.body.productId);
			let newProductsArray = [];
			newProductsArray = newProductsArray.concat(
				cart[0].products.slice(0, indexToRemove),
				cart[0].products.slice(indexToRemove + 1)
			);
			cart[0].products = newProductsArray;
		}

		//[SECTION] Change the quantity
		cart[0].products.forEach(productItem => {
			if(productItem.productId == request.body.productId){
				productItem.quantity = request.body.quantity;
			}
		});
		
		//[SECTION] Calculate the subtotal and total price
		let totalValue = 0;
		for(let productItem of cart[0].products){
			let current_product = await Product.findOne({_id: productItem.productId});
			if(current_product){
				productItem.subTotal = current_product.price * productItem.quantity;
				totalValue += productItem.subTotal;
			}
		}
	    cart[0].totalPrice = totalValue;

	    //[SECTION] Set inactive if the cart is empty
		if(cart[0].products.length == 0){
			cart[0].isActive = false;
		}

	    //[SECTION] Save the updated cart
		return cart[0].save().then(updated_cart => {
			return response.send({
				message: "The product's quantity changed successfully!"
			});
		}).catch(error => response.send(error));
	}).catch(error => response.send(error));
}


module.exports.getSubtotal = (request, response) => {
	Cart.find({userId: request.user.id, isActive: true}).then(cart => {
		if(cart.length == 0){
			return response.send({
				message: "The cart is empty."
			});
		}
		const filteredProductProperties = cart[0].products.map(product => {
			return {
				productName: product.productName,
				subTotal: product.subTotal
			}
		});
		return response.send({
			message: "Subtotal for each item",
			items: filteredProductProperties
		});
	}).catch(error => response.send(error));
}


module.exports.getTotalPrice = (request, response) => {
	Cart.find({userId: request.user.id, isActive: true}).then(cart => {
		if(cart.length == 0){
			return response.send({
				message: "The cart is empty."
			});
		}
		return response.send({
			totalPrice: cart[0].totalPrice
		});
	}).catch(error => response.send(error));
}


module.exports.getCart = (request, response) => {
	Cart.find({userId: request.user.id, isActive: true}).then(cart => {
		if(cart.length == 0){
			return response.send({
				message: "The cart is empty."
			});
		}
		return response.send(cart);
	}).catch(error => response.send(error));
}


module.exports.checkoutCart = (request, response) => {
	Cart.find({userId: request.user.id, isActive: true}).then(async cart => {
		//[SECTION] Check if there's any active cart
		if(cart.length == 0){
			return response.send({
				message: "The cart is empty."
			});
		}

		//[SECTION] Set the cart inactive
		cart[0].isActive = false;
		cart[0].save().then().catch(error => response.send(error));
		
		//[SECTION] Updating products' stocks
	    for(let productsArrayItem of cart[0].products){
			await Product.findOne({_id: productsArrayItem.productId}).then(current_product => {
				current_product.stock -= productsArrayItem.quantity;
				if(current_product.stock == 0) current_product.isActive = false;
				current_product.save().then().catch(error => response.send(error));
			}).catch(error => response.send(error));
		}

		//[SECTION] Create a new order
		const removedSubtotalArray = cart[0].products.map(product => {
			return {
				productId: product.productId,
				productName: product.productName,
				quantity: product.quantity
			}
		});
		let new_order = new Order({
			userId: cart[0].userId,
			userFullName: cart[0].userFullName,
			products: removedSubtotalArray,
			totalAmount: cart[0].totalPrice
		});
		return new_order.save().then(saved_order => {
			return response.send({
				message: "Thank you for your purchase!",
				receipt: saved_order
			});
		}).catch(error => response.send(error));
	}).catch(error => response.send(error));
}