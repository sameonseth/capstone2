const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");


module.exports.registerUser = (request, response) => {
	User.findOne({email: request.body.email}).then(result => {
		if(result){
			return response.send({
				status: "REGISTERED_USER"
			});
		}
		else{
			let new_user = new User({
				firstName: request.body.firstName,
				lastName: request.body.lastName,
				email: request.body.email,
				password: bcrypt.hashSync(request.body.password, 10),
				mobileNo: request.body.mobileNo
			});
			return new_user.save().then(registered_user => {
				return response.send({
					status: "OKAY"
				});
			}).catch(error => response.send(error));
		}
	}).catch(error => response.send(error));
}


module.exports.loginUser = (request, response) => {
	User.findOne({email: request.body.email}).then(user => {
		if(!user){
			return response.send({
				status: "INVALID_USER"
			});
		}
		const isPasswordCorrect = bcrypt.compareSync(request.body.password, user.password);
		if(isPasswordCorrect){
			return response.send({
				status: "OKAY",
				accessToken: auth.createAccessToken(user)
			});
		}
		else{
			return response.send({
				status: "INCORRECT_PASSWORD"
			});
		}
	}).catch(error => response.send(error));
}


module.exports.getAllUsers = (request, response) => {
	User.find({})
	.then(all_users => {
		if(all_users.length == 0){
			return response.send({
				status: "EMPTY_USERS"
			});
		}
		return response.send(all_users)
	})
	.catch(error => response.send(error));
}


module.exports.getProfile = (request, response) => {
	return User.findById(request.user.id).then(user => {
		return response.send(user);
	}).catch(error => console.log(error));
}


module.exports.setUserAsAdmin = (request, response) => {
	User.findById(request.params.id).then(user => {
		if(user && user.isAdmin == false){
			user.isAdmin = true;
			user.save().then().catch(error => response.send(error));
			return response.send({
				status: "OKAY"
			});
		}
		else if(user && user.isAdmin == true){
			return response.send({
				status: "ALREADY_ADMIN"
			});
		}
		else{
			return response.send({
				status: "INVALID_USER"
			});
		}
	}).catch(error => response.send(error));
}


module.exports.setAdminAsUser = (request, response) => {
	User.findById(request.params.id).then(user => {
		if(user && user.isAdmin == true){
			user.isAdmin = false;
			user.save().then().catch(error => response.send(error));
			return response.send({
				status: "OKAY"
			});
		}
		else if(user && user.isAdmin == false){
			return response.send({
				status: "ALREADY_USER"
			});
		}
		else{
			return response.send({
				status: "INVALID_USER"
			});
		}
	}).catch(error => response.send(error));
}


module.exports.changePassword = (request, response) => {
	return User.findById(request.user.id).then(user => {
		const isPasswordCorrect = bcrypt.compareSync(request.body.old_password, user.password);
		if(!isPasswordCorrect){
			return response.send({
				status: "INCORRECT_PASSWORD"
			});
		}
		user.password = bcrypt.hashSync(request.body.new_password, 10);
		user.save().then(updated_user => {
			return response.send({
				status: "OKAY"
			});
		}).catch(error => response.send(error));
	}).catch(error => console.log(error));
}