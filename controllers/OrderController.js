const Order = require("../models/Order.js");
const Product = require("../models/Product.js");
const User = require("../models/User.js");


module.exports.createOrder = async (request, response) => {
	//[SECTION] Checks if any of the products ordered is not available or if it doesn't exist at all in the database; Checks if the entered quantity is lesser than or equal to 0 or quantity is more than the stocks
	let isQuantityValid = true;
	let isQuantityMoreThanStock = false;
	let areAllProductsActive = true;
	for(let productsArrayItem of request.body.products){
		await Product.findOne({_id: productsArrayItem.productId}).then(current_product => {
			if(productsArrayItem.quantity <= 0) return isQuantityValid = false;
			if(productsArrayItem.quantity > current_product.stock) return isQuantityMoreThanStock = true;
			if(current_product && !current_product.isActive) return areAllProductsActive = false;
		}).catch(error => response.send(error));
	}
	if(!areAllProductsActive){
		return response.send({
			status: "INVALID_PRODUCT"
		});
	}
	if(!isQuantityValid){
		return response.send({
			status: "INVALID_QUANTITY"
		});
	}	
	if(isQuantityMoreThanStock){
		return response.send({
			status: "LIMITED_STOCK"
		});
	}


	//[SECTION] Getting the fullname of the user with a certain id
	let user = await User.findOne({_id: request.user.id});
	let fullname = `${user.firstName} ${user.lastName}`;

	//[SECTION] Calculate the total amount based on products and setting the productName for each product in products array
    let totalValue = 0;
    //Checks if request.body.products is an array
    if(request.body.products && Array.isArray(request.body.products)){
        //Iterate through each productsArrayItem in the products array
        for(let productsArrayItem of request.body.products){
            if(productsArrayItem.productId && productsArrayItem.quantity){
            	//Fetch the product price from the Product model
                let current_product = await Product.findOne({_id: productsArrayItem.productId});
                if(current_product){
                	//Calculate the total by multiplying current_product price and quantity
                    totalValue += current_product.price * productsArrayItem.quantity;
                    productsArrayItem.productName = current_product.name;
                }
            }
        }
    }

    //[SECTION] Updating products' stocks
    for(let productsArrayItem of request.body.products){
		await Product.findOne({_id: productsArrayItem.productId}).then(current_product => {
			current_product.stock -= productsArrayItem.quantity;
			if(current_product.stock == 0) current_product.isActive = false;
			current_product.save().then().catch(error => response.send(error));
		}).catch(error => response.send(error));
	}

    //[SECTION] Create and save new order
	let new_order = new Order({
		userId: request.user.id,
		userFullName: fullname,
		products: request.body.products,
		totalAmount: totalValue
	});
	return new_order.save().then(saved_order => {
		return response.send({
			status: "OKAY"
			//order: saved_order
		});
	}).catch(error => response.send(error));
}


module.exports.getCurrentUserOrders = (request, response) => {
	Order.find({userId: request.user.id}).sort({purchasedOn: -1}).then(user_orders => {
		if(user_orders.length == 0){
			return response.send({
				status: "EMPTY_ORDERS"
			});
		}
		return response.send(user_orders);
	}).catch(error => response.send(error));
}


module.exports.getAllUsersOrders = (request, response) => {
	Order.find({}).sort({purchasedOn: -1}).then(all_orders => {
		if(all_orders.length == 0){
			return response.send({
				status: "EMPTY_ORDERS"
			});
		}
		return response.send(all_orders);
	}).catch(error => response.send(error));
}