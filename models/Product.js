const mongoose = require("mongoose");

const product_schema = new mongoose.Schema({
	name: {
        type: String,
        required: [true, "Product is required!"]
    },
    description: {
        type: String,
        required: [true, "Description is required!"]
    },
    image: {
        type: String,
        required: [true, "Image is required!"]
    },
    price: {
        type: Number,
        required: [true, "Price is required!"]
    },
    stock: {
        type: Number,
        required: [true, "Stock is required!"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    }
});

module.exports = mongoose.model("Product", product_schema);