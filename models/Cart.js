const mongoose = require("mongoose");

const cart_schema = new mongoose.Schema({
	userId: {
        type: String,
        required: [true, "UserId is required!"]
    },
    userFullName: {
        type: String,
        required: false
    },
    products: [
        {
            productId: {
                type : String,
                required: [true, "ProductId is required!"]
            },
            productName: {
                type: String,
                required: false
            },
            quantity: {
                type: Number,
                required: [true, "Quantity is required!"]
            },
            subTotal: {
                type: Number,
                required: [true, "Subtotal is required!"]
            }
        }
    ],
    totalPrice: {
        type: Number,
        required: [true, "Total price is required!"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    }
});

module.exports = mongoose.model("Cart", cart_schema);